
import numpy as np
import lasagne
import theano
import theano.tensor as T
from AverageLayer import AverageLayer

np.random.seed(1)

W = np.random.uniform(0,1,(10,5))
print W

mat=T.imatrix()
mask=T.imatrix()

l_in = lasagne.layers.InputLayer((None, None, 1))
l_mask = lasagne.layers.InputLayer(shape=(None, None))
l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=10, output_size=5, W=W)
l_avg = AverageLayer([l_emb, l_mask])
emb = lasagne.layers.get_output(l_emb, {l_in:mat})
avg = lasagne.layers.get_output(l_avg, {l_in:mat, l_mask:mask})

data = np.array([[1,2,3,0],[2,3,4,0],[5,2,4,1]]).astype("int32")
data_mask = np.array([[1,1,1,0],[1,1,1,0],[1,1,1,1]]).astype("int32")

f1 = theano.function([mat],emb)
f2 = theano.function([mat,mask],avg)

d = f1(data)
print d, d.shape

print (d[0][0]+d[0][1]+d[0][2])/3.
print (d[1][0]+d[1][1]+d[1][2])/3.
print (d[2][0]+d[2][1]+d[2][2]+d[2][3])/4.

d = f2(data,data_mask)
print d, d.shape

