
from utils import getWordmap
from params import params
from utils import getData
from lstm_ppdb_model import lstm_ppdb_model
from lstm_ppdb_model_avg import lstm_ppdb_model_avg
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.LW = 1E-15
params.LC = 1E-15
params.frac = 0.1
params.outfile = 'test.out'
params.batchsize = 100
params.hiddensize = 25
params.type = "MAX"
params.updateword = False
params.save = False

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt","")
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+wordfilestem+".txt"
examples = getData(params.dataf,words)

params.data = examples[0:int(params.frac*len(examples))]

print "Saving to: "+params.outfile

#model = lstm_ppdb_model(We, params.hiddensize, params.LC, params.LW, params.margin,
#                                  params.updateword, params.eta)
model = lstm_ppdb_model_avg(We, params.hiddensize, params.LC, params.LW, params.margin,
                                  params.updateword, params.eta)
model.train(examples, words, params)

#sequences = [[1,2,3],[1,2],[1,2,3]]
#x,x_mask = lstm_ppdb_model.prepare_data(sequences)
#print lstm_ppdb_model.feedforward_function(x,x_mask)