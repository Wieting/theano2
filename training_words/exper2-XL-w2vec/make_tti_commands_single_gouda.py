import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
outfile = sys.argv[4]

lambda2=[0.000001,0.0000001,0.00000001,0.000000001,0.0000000001]
bs=[25,50,75,100,200]
type = ["MIX","MAX"]
margin = [0.4,0.6,0.8,1.0,2.0]

for j in range(len(bs)):
    for i in range(len(lambda2)):
        for k in range(len(type)):
            for l in margin:
                cmd = str(lambda2[i])+" "+frac+" "+fname+" "+data+" "+str(bs[j])+" "+str(l)+" 300 "+type[k]
                cmd = "sh exp1.train_gouda.sh "+cmd
                print cmd