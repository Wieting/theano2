
import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate import evaluate_all
import time
import utils
from collections import OrderedDict
import lasagne
import sys

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class elman_ppdb_model(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        return x, x_mask

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        embg1 = self.feedforward_function(g1x,g1mask)
        embg2 = self.feedforward_function(g2x,g2mask)

        #update representations
        for idx,i in enumerate(batch):
            i[0].representation = embg1[idx,:]
            i[1].representation = embg2[idx,:]

        pairs = utils.getPairs(batch, params.type)
        p1 = []; p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        return (g1x,g1mask,g2x,g2mask,p1x,p1mask,p2x,p2mask)

    def __init__(self, We_initial, D, LC, LW, margin, updatewords, eta):

        self.LC = LC
        self.LW = LW
        self.D = D

        #params
        initial_We = theano.shared(We_initial).astype(config.floatX)
        We = theano.shared(We_initial).astype(config.floatX)

        #RNN params
        #W_in_to_hid=np.identity(D)
        #W_hid_to_hid=np.identity(D)
        #b=np.zeros((D,))

        W_in_to_hid = lasagne.init.Normal(0.1)
        W_hid_to_hid = lasagne.init.Normal(0.1)
        b=lasagne.init.Constant(0.)

        #initial_params = np.concatenate((W_hid_to_hid.flatten(),W_hid_to_hid.flatten(),b))

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        p1batchindices = T.imatrix(); p2batchindices = T.imatrix()
        g1mask = T.matrix(); g2mask = T.matrix()
        p1mask = T.matrix(); p2mask = T.matrix()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None, 1))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=D, W=We)
        #l_rnn = lasagne.layers.RecurrentLayer(l_emb, D, mask_input = l_mask,
        #                                 W_in_to_hid=W_in_to_hid, W_hid_to_hid=W_hid_to_hid, b=b,
        #                                 nonlinearity=lasagne.nonlinearities.linear)

        l_rnn = lasagne.layers.RecurrentLayer(l_emb, D, mask_input = l_mask,
                                         W_in_to_hid=W_in_to_hid, W_hid_to_hid=W_hid_to_hid, b=b)
        l_out = lasagne.layers.SliceLayer(l_rnn, -1, 1)

        embg1 = lasagne.layers.get_output(l_out, {l_in:g1batchindices, l_mask:g1mask})
        embg2 = lasagne.layers.get_output(l_out, {l_in:g2batchindices, l_mask:g2mask})
        embp1 = lasagne.layers.get_output(l_out, {l_in:p1batchindices, l_mask:p1mask})
        embp2 = lasagne.layers.get_output(l_out, {l_in:p2batchindices, l_mask:p2mask})

        #objective function
        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(embg1**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        g1g2 = g1g2 / g1g2norm

        p1g1 = (embp1*embg1).sum(axis=1)
        p1g1norm = T.sqrt(T.sum(embp1**2,axis=1)) * T.sqrt(T.sum(embg1**2,axis=1))
        p1g1 = p1g1 / p1g1norm

        p2g2 = (embp2*embg2).sum(axis=1)
        p2g2norm = T.sqrt(T.sum(embp2**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        p2g2 = p2g2 / p2g2norm

        costp1g1 = margin - g1g2 + p1g1
        costp1g1 = costp1g1*(costp1g1 > 0)

        costp2g2 = margin - g1g2 + p2g2
        costp2g2 = costp2g2*(costp2g2 > 0)

        cost = costp1g1 + costp2g2
        network_params = lasagne.layers.get_all_params(l_rnn, trainable=True)
        network_params.pop(0)
        self.all_params = lasagne.layers.get_all_params(l_rnn, trainable=True)

        #regularization
        l2 = self.LC*sum(lasagne.regularization.l2(x) for x in network_params)
        #curr_params = T.concatenate((T.flatten(network_params[0]),T.flatten(network_params[2]),T.flatten(network_params[1])))
        #lspecial = lasagne.regularization.l2(initial_params-curr_params)

        if updatewords:
            word_reg = self.LW*lasagne.regularization.l2(We-initial_We)
            cost = T.mean(cost) + l2 + word_reg
            #cost = T.mean(cost) + lspecial + word_reg
        else:
            cost = T.mean(cost) + l2
            #cost = T.mean(cost) + lspecial

        #feedforward
        self.feedforward_function = theano.function([g1batchindices,g1mask], embg1)
        self.cost_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                             g1mask, g2mask, p1mask, p2mask], cost)


        #updates
        self.train_function = None
        if updatewords:
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                             g1mask, g2mask, p1mask, p2mask], cost, updates=updates)
        else:
            self.all_params = network_params
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                             g1mask, g2mask, p1mask, p2mask], cost, updates=updates)

    #trains parameters
    def train(self,data,words,params):
        start_time = time.time()
        evaluate_all(self,words)

        try:
            for eidx in xrange(params.epochs):
                n_samples = 0

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1
                    batch = [data[t] for t in train_index]
                    (g1x,g1mask,g2x,g2mask,p1x,p1mask,p2x,p2mask) = self.getpairs(batch, params)
                    cost = self.train_function(g1x, g2x, p1x, p2x, g1mask, g2mask, p1mask, p2mask)

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    if(checkIfQuarter(uidx,len(kf))):
                        #if(params.save):
                        #    counter += 1
                        #    saveParams(words,tm,params.outfile,counter)
                        if(params.evaluate):
                            evaluate_all(self,words)
                            sys.stdout.flush()

                    #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                evaluate_all(self,words)
                print 'Epoch ', (eidx+1), 'Cost ', cost

            print 'Seen %d samples' % n_samples


        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)