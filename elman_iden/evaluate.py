from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from utils import lookup_with_unk
from utils import lookup
from utils import getWordmap
from utils import lookupIDX
import numpy as np
import nltk
import math

def evaluateJHUPPDB(model,words):
    f = open('../dataset/ppdb-sample.tsv','r')
    lines = f.readlines()
    gold = []
    rnn_preds = []
    for i in lines:
        i=i.split("\t")
        score = float(i[0])
        p1 = i[2].lower()
        p2 = i[3].lower()
        if len(p1.split()) == 1 or len(p2.split())==1:
            continue
        gold.append(score)
        rnn_preds.append(RNN(p1,p2,model,words))
    return spearmanr(gold,rnn_preds)[0]

def getTokens(p1,p2,tokenize):
    t1 = nltk.word_tokenize(p1)
    t1 = tokenFix(t1)
    if len(t1) == 0 or tokenize == False:
        t1 = p1.split()
    t2 = nltk.word_tokenize(p2)
    t2 = tokenFix(t2)
    if len(t2) == 0 or tokenize == False:
        t2 = p2.split()
    return t1, t2

def tokenFix(toks):
    arr = []
    for i in toks:
        if len(i) > 1:
            i = i.replace(",","")
            arr.append(i)
    return arr

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff

    if xdiff2 == 0 or ydiff2 == 0:
        return 0.0

    return diffprod / math.sqrt(xdiff2 * ydiff2)

def EvalSingleSystem(testlabelfile, sysscores):

    # read in golden labels
    goldlabels = []
    goldscores = []

    hasscore = False
    with open(testlabelfile) as tf:
        for tline in tf:
            tline = tline.strip()
            tcols = tline.split('\t')
            if len(tcols) == 2:
                goldscores.append(float(tcols[1]))
                if tcols[0] == "true":
                    goldlabels.append(True)
                elif tcols[0] == "false":
                    goldlabels.append(False)
                else:
                    goldlabels.append(None)

    tp = 0
    fn = 0
    # evaluation metrics
    for i in range(len(goldlabels)):

        if goldlabels[i] == True:
            tp += 1

    # system degreed scores vs golden binary labels
    # maxF1 / Precision / Recall

    maxF1 = 0
    P_maxF1 = 0
    R_maxF1 = 0

    # rank system outputs according to the probabilities predicted
    sortedindex = sorted(range(len(sysscores)), key = sysscores.__getitem__)
    sortedindex.reverse()

    truepos  = 0
    falsepos = 0

    for sortedi in sortedindex:
        if goldlabels[sortedi] == True:
            truepos += 1
        elif goldlabels[sortedi] == False:
            falsepos += 1

        precision = 0

        if truepos + falsepos > 0:
            precision = float(truepos) / (truepos + falsepos)

        recall = float(truepos) / (tp + fn)
        f1 = 0

        #print truepos, falsepos, precision, recall

        if precision + recall > 0:
            f1 = 2 * precision * recall / (precision + recall)
            if f1 > maxF1:
                maxF1 = f1
                P_maxF1 = precision
                R_maxF1 = recall

    # system degreed scores  vs golden degreed scores
    # Pearson correlation
    #print len(sysscores), len(goldscores)
    pcorrelation = pearson(sysscores, goldscores)

    return (pcorrelation, maxF1)

def evaluateTwitter(model,words):
    file = open('../dataset/test.data','r')
    lines = file.readlines()
    rnn_preds = []
    for i in lines:
        i =i.split("\t")
        s1 = i[2].lower()
        s2 = i[3].lower()
        rnn_preds.append(RNN(s1,s2,model,words))
    (rnn_c, rnn_f) = EvalSingleSystem("../dataset/test.label",rnn_preds)
    return (rnn_c, rnn_f)

def getwordsim(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[2]))
            examples.append(ex)
    return examples

def getsimlex(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[3]))
            examples.append(ex)
    return examples

def getCorr(examples, We, words):
    gold = []
    pred = []
    num_unks = 0
    for i in examples:
        (v1,t1) = lookup_with_unk(We,words,i[0])
        (v2,t2) = lookup_with_unk(We,words,i[1])
        #print v1,v2
        pred.append(-1*cosine(v1,v2)+1)
        if t1:
            num_unks += 1
            #print i[0]
        if t2:
            num_unks += 1
        gold.append(i[2])
    return (spearmanr(pred,gold)[0], num_unks)

def evaluateWordSim(We, words):
    ws353ex = getwordsim('../dataset/wordsim353.txt')
    ws353sim = getwordsim('../dataset/wordsim-sim.txt')
    ws353rel = getwordsim('../dataset/wordsim-rel.txt')
    simlex = getsimlex('../dataset/SimLex-999.txt')
    (c1,u1) = getCorr(ws353ex,We,words)
    (c2,u2) = getCorr(ws353sim,We,words)
    (c3,u3) = getCorr(ws353rel,We,words)
    (c4,u4) = getCorr(simlex,We,words)
    return ([c1,c2,c3,c4],[u1,u2,u3,4])

def RNN(p1,p2,model,words):
    p1,p2 = getTokens(p1,p2,True)
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1,X2]
    x,m = model.prepare_data(seqs)
    emb = model.feedforward_function(x,m)
    v1 = emb[0,:]
    v2 = emb[1,:]
    return -1*cosine(v1,v2)+1

def scoreannoppdb(f,model,words):
    f = open(f,'r')
    lines = f.readlines()
    rnn_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.split('|||')
        (p1,p2,score) = (i[0].strip(),i[1].strip(),float(i[2]))
        rnn_preds.append(RNN(p1,p2,model,words))
        gold.append(score)
    return spearmanr(rnn_preds,gold)[0]

def scoreSE(f,model,words):
    f = open(f,'r')
    lines = f.readlines()
    lines.pop(0)
    rnn_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.split('\t')
        (p1,p2,score) = (i[1].strip(),i[2].strip(),float(i[3]))
        rnn_preds.append(RNN(p1,p2,model,words))
        gold.append(score)
    return spearmanr(rnn_preds,gold)[0]

def evaluateAnno(model, words):
    return scoreannoppdb('../dataset/ppdb_test.txt',model,words)

def evaluateAnnoDev(model, words):
    return scoreannoppdb('../dataset/ppdb_dev.txt',model,words)

def evaluateSE(model, words):
    return scoreSE('../dataset/SICK_trial.txt',model,words)

def evaluate_all(model,words):
    #(corr, unk) = evaluateWordSim(model,words)
    rnn_s = evaluateAnno(model,words)
    rnn_sd = evaluateAnnoDev(model,words)
    rnn_se = evaluateSE(model,words)
    rnn_sp = evaluateJHUPPDB(model,words)
    (c_rnn, f1_rnn) = evaluateTwitter(model,words)
    s = "{0:.4f} {1:.4f} | {2:.4f} | {3:.4f} | {4:.4f} {5:.4f} | ppdb_dev ppdb_test | se_rnn | jhurnn | twitter_rnn_sp twitter_rnn_maxf1 |".format(rnn_sd, rnn_s, rnn_se, rnn_sp, c_rnn, f1_rnn)
    print s

