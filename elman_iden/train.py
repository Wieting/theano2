from utils import getWordmap
from params import params
from utils import getData
from elman_ppdb_model import elman_ppdb_model
import random
import numpy as np
import sys

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.LW=float(args[1])
params.LC = float(args[2])
params.frac = float(args[3])
params.outfile = args[4]
params.dataf = args[5]
params.batchsize = int(args[6])
params.hiddensize = int(args[7])
params.wordfile = args[8]
params.updateword = False
params.type = args[9]
params.margin = float(args[10])
wordstem = args[11]
params.updateword = False
params.save = True

(words, We) = getWordmap(params.wordfile)
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+str(params.margin)+"."+str(params.hiddensize)+"."+wordstem+".txt"
examples = getData(params.dataf,words)

params.data = examples[0:int(params.frac*len(examples))]

print args
#print "Training on "+str(len(params.data))+" examples using lambda We ="+str(params.LW)\
#      +" and lambda C of ="+str(params.LC)+" with margin ="+str(params.margin)
print "Saving to: "+params.outfile

elman_ppdb_model = elman_ppdb_model(We, params.hiddensize, params.LC, params.LW, params.margin, params.updateword, params.eta)
elman_ppdb_model.train(examples, words, params)