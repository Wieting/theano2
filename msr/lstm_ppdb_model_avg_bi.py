
import theano
import numpy as np
from theano import tensor as T
from theano import config
from AverageLayer import AverageLayer
import pdb
from evaluate import evaluate_all
import time
import utils
from collections import OrderedDict
import lasagne
import sys

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class lstm_ppdb_model_avg_bi(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        return x, x_mask

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getData(self, batch):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        scores = []
        for i in batch:
            scores.append(i[2])

        return (scores,g1x,g1mask,g2x,g2mask)

    def __init__(self, We_initial, D, LC, LW, updatewords, eta):

        self.LC = LC
        self.LW = LW
        self.D = D

        #params
        initial_We = theano.shared(We_initial).astype(config.floatX)
        We = theano.shared(We_initial).astype(config.floatX)

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        g1mask = T.matrix(); g2mask = T.matrix()
        scores = T.dvector()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
        l_forward = lasagne.layers.LSTMLayer(l_emb, D, peepholes=False, mask_input = l_mask)
        l_backward = lasagne.layers.LSTMLayer(l_emb, D, peepholes=False, mask_input=l_mask, backwards=True)
        l_forward_slice = lasagne.layers.SliceLayer(l_forward, -1, 1)
        l_backward_slice = lasagne.layers.SliceLayer(l_backward, 0, 1)
        l_average = AverageLayer([l_emb, l_mask])
        l_sum = lasagne.layers.ConcatLayer([l_forward_slice, l_backward_slice, l_average])
        l_out = lasagne.layers.DenseLayer(l_sum, D, nonlinearity=lasagne.nonlinearities.tanh)

        embg1 = lasagne.layers.get_output(l_out, {l_in:g1batchindices, l_mask:g1mask})
        embg2 = lasagne.layers.get_output(l_out, {l_in:g2batchindices, l_mask:g2mask})

        #objective function
        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(embg1**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        g1g2 = g1g2 / g1g2norm

        cost = (scores - g1g2)**2
        network_params = lasagne.layers.get_all_params(l_out, trainable=True)
        network_params.pop(0)
        self.all_params = lasagne.layers.get_all_params(l_out, trainable=True)

        #regularization
        l2 = self.LC*sum(lasagne.regularization.l2(x) for x in network_params)
        if updatewords:
            word_reg = self.LW*lasagne.regularization.l2(We-initial_We)
            cost = T.mean(cost) + l2 + word_reg
        else:
            cost = T.mean(cost) + l2

        #feedforward
        self.feedforward_function = theano.function([g1batchindices,g1mask], embg1)
        self.cost_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost)


        #updates
        self.train_function = None
        if updatewords:
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)
        else:
            self.all_params = network_params
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)

    #trains parameters
    def train(self,data,words,params):
        start_time = time.time()
        evaluate_all(self,words)

        try:
            for eidx in xrange(params.epochs):
                n_samples = 0

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1
                    batch = [data[t] for t in train_index]
                    (scores,g1x,g1mask,g2x,g2mask) = self.getData(batch)
                    cost = self.train_function(scores, g1x, g2x, g1mask, g2mask)

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                evaluate_all(self,words)
                print 'Epoch ', (eidx+1), 'Cost ', cost

            print 'Seen %d samples' % n_samples


        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)