import nltk

def lookup(words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        return words['UUUNKKK']

def getTokens(p1):
    t1 = nltk.word_tokenize(p1)
    t1 = tokenFix(t1)
    return t1

def tokenFix(toks):
    arr = []
    for i in toks:
        if len(i) > 1:
            i = i.replace(",","")
            arr.append(i)
    return arr

class tree(object):

    def __init__(self, phrase, words):
        self.phrase = phrase
        self.embeddings = []
        self.populate_embeddings(words)
        self.representation = None

    def populate_embeddings(self, words):
        phrase = self.phrase.lower()
        arr = getTokens(phrase)
        for i in arr:
            self.embeddings.append(lookup(words,i))