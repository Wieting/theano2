from utils import *
from params import params
from lstm_ppdb_model import lstm_ppdb_model
from lstm_ppdb_model_bi import lstm_ppdb_model_bi
from lstm_ppdb_model_avg import lstm_ppdb_model_avg
from lstm_ppdb_model_avg_bi import lstm_ppdb_model_avg_bi
import random
import numpy as np
import sys

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.LW=float(args[1])
params.LC = float(args[2])
params.outfile = args[3]
params.batchsize = int(args[4])
params.hiddensize = int(args[5])
params.wordfile = args[6]
type = args[7]
params.layersize = int(args[8])
wordstem = args[9]
params.updateword = False
params.save = True

(words, We) = getWordmap(params.wordfile)
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+str(params.layersize)+"."+str(params.hiddensize)+"."+wordstem+".txt"
examples = getSICKtrain(words)

params.data = examples

print args
#print "Training on "+str(len(params.data))+" examples using lambda We ="+str(params.LW)\
#      +" and lambda C of ="+str(params.LC)+" with margin ="+str(params.margin)
print "Saving to: "+params.outfile

model = None

if type == 'lstm':
    model = lstm_ppdb_model(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'bi':
    model = lstm_ppdb_model_bi(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'avg':
    model = lstm_ppdb_model_avg(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'bi_avg':
    model = lstm_ppdb_model_avg_bi(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)
else:
    "Error no type specified"

model.train(examples, words, params)