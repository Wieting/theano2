import numpy as np
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform

mat = np.matrix([[2,3],[4,5],[6,7],[5,6],[2,7],[9,0]])
arr = pdist(mat,'cosine')
arr = squareform(arr)

for i in range(len(mat)):
    arr[i,i]=1
    if i % 2 == 0:
        arr[i,i+1] = 1
    else:
        arr[i,i-1] = 1

arr = np.argmin(arr,axis=1)