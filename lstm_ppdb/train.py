from utils import getWordmap
from params import params
from utils import getData
from lstm_ppdb_model import lstm_ppdb_model
from lstm_ppdb_model_extra import lstm_ppdb_model_extra
from lstm_ppdb_model_extra_concatenate import lstm_ppdb_model_extra_concatenate
from lstm_ppdb_model_concatenate import lstm_ppdb_model_concatenate
import random
import numpy as np
import sys

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.LW=float(args[1])
params.LC = float(args[2])
params.outfile = args[3]
params.batchsize = int(args[4])
params.hiddensize = int(args[5])
params.memsize = int(args[6])
params.wordfile = args[7]
params.nntype = args[8]
params.layersize = int(args[9])
params.updateword = False
params.wordstem = args[10]
params.save = False
params.dataf = args[11]
params.type = args[12]
params.margin = float(args[13])
params.peephole = args[14]=='True'
params.outgate = args[15]=='True'
params.weight = False
if len(args) > 16:
    params.weight = args[16]=='True'

(words, We) = getWordmap(params.wordfile)
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+str(params.margin)+"."+str(params.hiddensize)+"."+params.wordstem+".txt"
examples = getData(params.dataf,words)

params.data = examples

print args
#print "Training on "+str(len(params.data))+" examples using lambda We ="+str(params.LW)\
#      +" and lambda C of ="+str(params.LC)+" with margin ="+str(params.margin)
print "Saving to: "+params.outfile

model = None

if params.nntype == 'lstm':
    model = lstm_ppdb_model(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
elif params.nntype == 'extra':
    model = lstm_ppdb_model_extra(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
elif params.nntype == 'concat':
    model = lstm_ppdb_model_concatenate(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate, params.weight)
elif params.nntype == 'extra_concat':
    model = lstm_ppdb_model_extra_concatenate(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
else:
    "Error no type specified"

model.train(examples, words, params)