
from utils import getWordmap
from params import params
from utils import getData
from lstm_ppdb_model import lstm_ppdb_model
from lstm_ppdb_model_extra import lstm_ppdb_model_extra
from lstm_ppdb_model_concatenate import lstm_ppdb_model_concatenate
#from lstm_ppdb_model_extra_concatenate import lstm_ppdb_model_extra_concatenate
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.LW=0.
params.LC = 1E-5
params.outfile = 'test.out'
params.batchsize = 100
params.hiddensize = 300
params.memsize = 100
params.wordfile = '../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt'
params.nntype = 'lstm'
params.layersize = 300
params.updateword = False
params.wordstem = 'simlex'
params.save = False
params.dataf = '../data/phrase_pairs'
params.type = 'MAX'
params.margin = 0.2
params.peephole = False
params.outgate = False

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt","")
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+str(params.margin)+"."+str(params.hiddensize)+"."+params.wordstem+".txt"
examples = getData(params.dataf,words)

params.data = examples[0:int(0.1*len(examples))]

print "Saving to: "+params.outfile

model = None

if params.nntype == 'lstm':
    model = lstm_ppdb_model(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
elif params.nntype == 'extra':
    model = lstm_ppdb_model_extra(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
elif params.nntype == 'concat':
    model = lstm_ppdb_model_concatenate(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate, True)
#elif params.nntype == 'extra_concat':
#    model = lstm_ppdb_model_extra_concatenate(We, params.layersize, params.memsize, params.LC, params.LW,
#                                  params.updateword, params.eta, params.margin, params.peephole, params.outgate)
else:
    "Error no type specified"

model.train(examples, words, params)
