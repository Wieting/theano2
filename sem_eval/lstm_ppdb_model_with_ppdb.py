
import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate import evaluate_all
import time
import utils
from collections import OrderedDict
from LSTMLayerNoOutput import LSTMLayerNoOutput
import lasagne
import sys
import cPickle

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class lstm_ppdb_model_extra(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        return x, x_mask

    def saveParams(self, fname, info):
        f = file(fname, 'wb')
        cPickle.dump(self.network_params, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getData(self, batch):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        scores = []
        for i in batch:
            temp = np.zeros(5)
            score = float(i[2])
            ceil, fl = int(np.ceil(score)), int(np.floor(score))
            if ceil == fl:
                temp[fl-1] = 1
            else:
                temp[fl-1] = ceil - score
                temp[ceil-1] = score - fl
            #print temp, score
            scores.append(temp)
        scores = np.matrix(scores)+0.000001
        #print scores
        return (scores,g1x,g1mask,g2x,g2mask)

    def __init__(self, We_initial, layersize, memsize, LC, LW, updatewords, eta, usepeep, useoutgate):

        self.LC = LC
        self.LW = LW
        self.L = layersize
        self.M = memsize
        self.usepeep = usepeep
        self.useoutgate = useoutgate
        #self.initialWE = We_initial

        #params
        initial_We = theano.shared(We_initial).astype(config.floatX)
        We = theano.shared(We_initial).astype(config.floatX)

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        g1mask = T.matrix(); g2mask = T.matrix()
        scores = T.dmatrix()

        #init gates
        #init_f = lasagne.init.Uniform(range=1./np.sqrt(layersize))
        #ingate=lasagne.layers.Gate(W_in=init_f, W_hid=init_f, W_cell=init_f, b=init_f)
        #forgetgate=lasagne.layers.Gate(W_in=init_f, W_hid=init_f, W_cell=init_f, b=init_f)
        #cell=lasagne.layers.Gate(W_in=init_f, W_hid=init_f, W_cell=init_f, b=init_f, nonlinearity=lasagne.nonlinearities.tanh)
        #outgate=lasagne.layers.Gate(W_in=init_f, W_hid=init_f, W_cell=init_f, b=init_f)

        #init other paramters

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
        l_lstm = None
        if useoutgate:
            l_lstm = lasagne.layers.LSTMLayer(l_emb, layersize, peepholes=usepeep, learn_init=False, mask_input = l_mask)
        else:
            l_lstm = LSTMLayerNoOutput(l_emb, layersize, peepholes=usepeep, learn_init=False, mask_input = l_mask)

        #l_lstm = lasagne.layers.recurrent.LSTMLayerNoOutputGate(l_emb, layersize, peepholes=False, learn_init=False, mask_input = l_mask)
        #l_lstm = lasagne.layers.LSTMLayer(l_emb, layersize, peepholes=False, learn_init=False, mask_input = l_mask,
        #                                  ingate=ingate, forgetgate=forgetgate,cell=cell,outgate=outgate)
        l_forward_slice = lasagne.layers.SliceLayer(l_lstm, -1, 1)

        embg1 = lasagne.layers.get_output(l_forward_slice, {l_in:g1batchindices, l_mask:g1mask})
        embg2 = lasagne.layers.get_output(l_forward_slice, {l_in:g2batchindices, l_mask:g2mask})

        #objective function
        g1_dot_g2 = embg1*embg2
        g1_abs_g2 = abs(embg1-embg2)

        lin_dot = lasagne.layers.InputLayer((None, layersize))
        lin_abs = lasagne.layers.InputLayer((None, layersize))
        l_sum = lasagne.layers.ConcatLayer([lin_dot, lin_abs])
        l_sigmoid = lasagne.layers.DenseLayer(l_sum, memsize, nonlinearity=lasagne.nonlinearities.sigmoid)
        l_softmax = lasagne.layers.DenseLayer(l_sigmoid, 5, nonlinearity=T.nnet.softmax)
        X = lasagne.layers.get_output(l_softmax, {lin_dot:g1_dot_g2, lin_abs:g1_abs_g2})

        #self.X_f = theano.function([g1batchindices, g2batchindices,
        #                     g1mask, g2mask],X)

        Y = T.log(X)
        #self.Y_f = theano.function([g1batchindices, g2batchindices,
        #                     g1mask, g2mask],Y)

        cost = scores*(T.log(scores) - Y)
        #self.cost1_f = theano.function([scores, g1batchindices, g2batchindices,
        #                     g1mask, g2mask],cost)

        cost = cost.sum(axis=1)/5.
        #self.cost2_f = theano.function([scores, g1batchindices, g2batchindices,
        #                     g1mask, g2mask],cost)

        #targ = T.matrix()
        #pred = T.matrix()
        #temp = targ*(T.log(targ) - pred)
        #self.special_cost = theano.function([targ,pred],temp)

        prediction = 1*X[:,0]+2*X[:,1]+3*X[:,2]+4*X[:,3]+5*X[:,4]
        #cost = (scores - output)**2
        #cost = X * T.log(X / scores) + (1 - X) * T.log((1 - X) / (1 - scores))

        #pdb.set_trace()

        self.network_params = lasagne.layers.get_all_params(l_lstm, trainable=True) + lasagne.layers.get_all_params(l_softmax, trainable=True)
        self.network_params.pop(0)
        self.all_params = lasagne.layers.get_all_params(l_lstm, trainable=True) + lasagne.layers.get_all_params(l_softmax, trainable=True)
        #pdb.set_trace()
        print self.network_params

        #regularization
        l2 = 0.5*self.LC*sum(lasagne.regularization.l2(x) for x in self.network_params)
        if updatewords:
            word_reg = self.LW*lasagne.regularization.l2(We-initial_We)
            cost = T.mean(cost) + l2 + word_reg
        else:
            cost = T.mean(cost) + l2

        #temporary testing functions
        #self.output = theano.function([g1batchindices, g2batchindices,
        #                     g1mask, g2mask], output)

        #feedforward
        self.feedforward_function = theano.function([g1batchindices,g1mask], embg1)
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
                             g1mask, g2mask],prediction)
        self.cost_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost)

        #updates
        self.train_function = None
        if updatewords:
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)
        else:
            self.all_params = self.network_params
            updates = lasagne.updates.adagrad(cost, self.all_params, eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)

    #trains parameters
    def train(self,data,words,params):
        start_time = time.time()
        evaluate_all(self,words)
        counter = 1

        info = {}
        info['LC'] = self.LC
        info['L'] = self.L
        info['M'] = self.M
        info['words'] = params.wordstem

        try:
            for eidx in xrange(params.epochs):
                n_samples = 0

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1
                    batch = [data[t] for t in train_index]
                    (scores,g1x,g1mask,g2x,g2mask) = self.getData(batch)
                    #pdb.set_trace()
                    cost = self.train_function(scores,
                                               g1x, g2x, g1mask, g2mask)

                    #print intermediates
                    #print "Y_f"
                    #print self.Y_f(g1x, g2x, g1mask, g2mask)

                    #print "cost1_f"
                    #print self.cost1_f(scores,
                    #                           g1x, g2x, g1mask, g2mask)

                    #print "cost2_f"
                    #print self.cost2_f(scores,
                    #                          g1x, g2x, g1mask, g2mask)

                    #pdb.set_trace()

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                evaluate_all(self,words)
                self.saveParams(params.outfile+str(counter)+'.pickle',info)
                counter += 1
                print 'Epoch ', (eidx+1), 'Cost ', cost
                sys.stdout.flush()

            print 'Seen %d samples' % n_samples


        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)