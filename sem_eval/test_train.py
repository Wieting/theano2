from utils import *
from params import params
from lstm_ppdb_model import lstm_ppdb_model
from lstm_ppdb_model_bi import lstm_ppdb_model_bi
from lstm_ppdb_model_extra import lstm_ppdb_model_extra
from lstm_ppdb_model_avg import lstm_ppdb_model_avg
from lstm_ppdb_model_avg_bi import lstm_ppdb_model_avg_bi
import random
import numpy as np
import sys
import pdb

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.LW=0.
params.LC = 1E-5
params.outfile = 'test.out'
params.batchsize = 25
params.hiddensize = 300
type = 'extra'
params.layersize = 300
params.memsize = 300
#params.wordfile = '../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt'
params.wordfile = '../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt'
#params.wordfile = '../data/core_words.txt'
params.wordstem = 'glove'
params.updateword = False
params.save = False
params.peephole = True
params.outgate = False

(words, We) = getWordmap(params.wordfile)
params.outfile = "../models/"+params.outfile+"."+str(params.LW)+"."+str(params.LC)+"."+str(params.batchsize)+"."+params.type+\
                 "."+str(params.layersize)+"."+str(params.hiddensize)+"."+params.wordstem+".txt"
examples = getSICKtrain(words)
#examples = examples[0:2]

params.data = examples

print args
#print "Training on "+str(len(params.data))+" examples using lambda We ="+str(params.LW)\
#      +" and lambda C of ="+str(params.LC)+" with margin ="+str(params.margin)
print "Saving to: "+params.outfile

#pdb.set_trace()
model = None

if type == 'lstm':
    model = lstm_ppdb_model(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'bi':
    model = lstm_ppdb_model_bi(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'avg':
    model = lstm_ppdb_model_avg(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)

elif type == 'bi_avg':
    model = lstm_ppdb_model_avg_bi(We, params.layersize, params.LC, params.LW,
                                  params.updateword, params.eta)
elif type == 'extra':
    model = lstm_ppdb_model_extra(We, params.layersize, params.memsize, params.LC, params.LW,
                                  params.updateword, params.eta, params.peephole, params.outgate)
else:
    "Error no type specified"

model.train(examples, words, params)