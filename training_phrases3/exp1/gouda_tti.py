import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/phrase_pairs'
mem = [50,100,200]
lamC = [1E-4,1E-5,1E-6]
bs = [100,200,400]
margins = [0.3,0.45,0.6]
hidden = [75,150,225,300]
types = ['MAX','MIX']


for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for t in types:
                    vars = (lamC[l],fname,bs[j],dim,50,wordfile,'lstm',hidden[h],wordstem,data,t,margins[c])
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True".format(*vars)
                    print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False".format(*vars)
                    #print cmd

for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for m in range(len(mem)):
                    for t in types:
                        vars = (lamC[l],fname,bs[j],dim,mem[m],wordfile,'extra',hidden[h],wordstem,data,t,margins[c])
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False".format(*vars)
                        #print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True".format(*vars)
                        #print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False".format(*vars)
                        #print cmd

for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for t in types:
                    vars = (lamC[l],fname,bs[j],dim,mem[m],wordfile,'concat',hidden[h],wordstem,data,t,margins[c])
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True False".format(*vars)
                    print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False False".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True False".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False False".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True True".format(*vars)
                    print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False True".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True True".format(*vars)
                    #print cmd
                    cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False True".format(*vars)
                    #print cmd

for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for m in range(len(mem)):
                    for t in types:
                        vars = (lamC[l],fname,bs[j],dim,mem[m],wordfile,'extra_concat',hidden[h],wordstem,data,t,margins[c])
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True".format(*vars)
                        #print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False".format(*vars)
                        #print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True".format(*vars)
                        #print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False".format(*vars)
                        #print cmd