python gouda_tti.py sem_eval "../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt" "paragram_simlex" 300 > semeval_commands_gouda.txt

python gouda_tti.py sem_eval "../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt" "paragram_wordsim" 300 >> semeval_commands_gouda.txt

python gouda_tti.py sem_eval "../data/core_words.txt" "glove" 300 >> semeval_commands_gouda.txt
