import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = sys.argv[4]

lambdaWe=[1E-5]
lamC = [1E-4,1E-5,1E-6,1E-7]
bs = [25,50,100]
type = ['lstm','bi','avg','bi_avg']
hidden = [100,150,200,250,300]

for j in range(len(bs)):
    for i in range(len(lambdaWe)):
        for k in range(len(type)):
            for l in range(len(lamC)):
                for c in range(len(hidden)):
                    vars = (lambdaWe[i],lamC[l],fname,bs[j],dim,wordfile,type[k],hidden[c],wordstem)
                    cmd = "sh train_sem_eval.sh {0} {1} {2} {3} {4} {5} {6} {7} {8}".format(*vars)
                    print cmd
