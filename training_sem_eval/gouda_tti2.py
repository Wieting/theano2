import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = sys.argv[4]

lamC = [1E-3,1E-4,1E-5,1E-6]
bs = [25,50,100,200,500]
hidden = [100,150,200,250,300]
mem = [25,50,100,150,200,250,300]

for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(hidden)):
            for m in range(len(mem)):
                vars = (1E-5,lamC[l],fname,bs[j],dim,mem[m],wordfile,'extra',hidden[c],wordstem)
                cmd = "sh train_sem_eval.sh {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} True False".format(*vars)
                print cmd
                vars = (1E-5,lamC[l],fname,bs[j],dim,mem[m],wordfile,'extra',hidden[c],wordstem)
                cmd = "sh train_sem_eval.sh {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} False False".format(*vars)
                print cmd
