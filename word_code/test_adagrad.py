from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from adagrad import adagrad
import warnings
import numpy as np
import random

def limitwords(words,We,d):
    lis = []
    for i in d:
        lis.append(i[0])
        lis.append(i[1])
    lis = list(set(lis))
    newwords = {}
    for (n,i) in enumerate(lis):
        newwords[i]=n
    newWe = []
    for i in lis:
        newWe.append(np.array(We[words[i],:]).tolist()[0])
    return (newwords,np.matrix(newWe))

np.random.seed(1)
random.seed(1)
warnings.filterwarnings("ignore")
hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
examples = getData(params.dataf)
sample = examples[0:5000]
#(words,We) = limitwords(words,We,sample)

print "Testing adagrad"
params.data = sample
params.epochs=20
params.eta=0.20
params.batchsize=75
adagrad(params,words,We)