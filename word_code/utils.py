from scipy.io import loadmat
import numpy as np
from scipy.spatial.distance import cosine

def lookup(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:]
    else:
        return We[words['UUUNKKK'],:]

def lookupIDX(We,words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        return words['UUUNKKK']

def lookup_with_unk(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:],False
    else:
        return We[words['UUUNKKK'],:],True

def getData(f,rng):
    data = open(f,'r')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            e = (i[0], i[1], float(i[2]))
            examples.append(e)
    rng.shuffle(examples)
    return examples

def getWordmap(textfile):
    words={}
    We = []
    n=0
    for line in open(textfile):
        i=line.split()
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=n
        We.append(v)
        n += 1
    return (words, np.matrix(We))

def getPairMax(label,vec,idx,d,We,words,wi,wj):
    min = -5000
    best = None
    for i in range(len(d)):
        if i == idx:
            continue
        (w1,w2,l) = d[i]
        v1 = lookup(We,words,w1)
        v2 = lookup(We,words,w2)
        np1 = -cosine(v1,vec) + 1
        np2 = -cosine(v2,vec) + 1
        if(np1 > min and not(wi == w1) and not(wj==w1)):
            min = np1
            best = w1
        if(np2 > min and not(wi == w2) and not(wj==w2)):
            min = np2
            best = w2
    return best

def getPairRand(label,vec,idx,d,We,words,wi,wj,rng):
    wpick = None
    while(wpick == None or wpick == wi or wpick == wj):
        ww = rng.choice(d)
        ridx = rng.randint(0,1)
        wpick = ww[ridx]
        #print wpick
    return wpick

def getPairMix(label,vec,idx,d,We,words,wi,wj,rng):
    r1 = rng.randint(0,1)
    if r1 == 1:
        return getPairMax(label,vec,idx,d,We,words,wi,wj)
    else:
        return getPairRand(label,vec,idx,d,We,words,wi,wj,rng)

def getPairs(d, words, We, type,rng):
    pairs = []
    for i in range(len(d)):
        (w1,w2,l) = d[i]
        v1 = lookup(We,words,w1)
        v2 = lookup(We,words,w2)
        p1 = None
        p2 = None
        if type == "MAX":
            #print w1
            p1 = getPairMax(l,v1,i,d,We,words,w1,w2)
            #print w2
            p2 = getPairMax(l,v2,i,d,We,words,w2,w1)
        if type == "RAND":
            #print w1
            p1 = getPairRand(l,v1,i,d,We,words,w1,w2,rng)
            #print w2
            p2 = getPairRand(l,v2,i,d,We,words,w2,w1,rng)
        if type == "MIX":
            #print w1
            p1 = getPairMix(l,v1,i,d,We,words,w1,w2,rng)
            #print w2
            p2 = getPairMix(l,v2,i,d,We,words,w2,w1,rng)
        pairs.append((p1,p2))
    return pairs

def getPairsBatch(d, words, We, batchsize, type, rng):
    idx = 0
    pairs = []
    while idx < len(d):
        batch = d[idx: idx + batchsize if idx + batchsize < len(d) else len(d)]
        if(len(batch) <= 2):
            print "batch too small."
            continue #just move on because pairing could go faulty
        p = getPairs(batch,words,We,type,rng)
        pairs.extend(p)
        idx += batchsize
    return pairs

def convertToIndex(e,words, We):
    if len(e) == 3:
        (p1,p2,s) = e
        new_e = (lookupIDX(We, words, p1), lookupIDX(We, words, p2), int(float(s)))
        return new_e
    else:
        (p1,p2) = e
        new_e = (lookupIDX(We, words, p1), lookupIDX(We, words, p2))
        return new_e

def makeBatch(newd,newp):
    g1 = []
    g2 = []
    p1 = []
    p2 = []
    ll = []
    for i in newd:
        g1.append(i[0])
        g2.append(i[1])
        ll.append(i[2])
    for i in newp:
        p1.append(i[0])
        p2.append(i[1])
    return g1, g2, p1, p2, ll



