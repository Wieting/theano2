from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from utils import lookup_with_unk
from utils import lookup
from utils import getWordmap
import numpy as np
import nltk
import math

def processJHUPPDB():
    JHUall = []
    JHUwords = []
    JHUphrases = []
    f = open('../dataset/ppdb-sample.tsv','r')
    lines = f.readlines()
    for i in lines:
        i=i.split("\t")
        score = float(i[0])
        p1 = i[2].lower()
        p2 = i[3].lower()
        if len(p1.split()) == 1 and len(p2.split()) ==1:
            #print p1, p2
            JHUwords.append((p1,p2,score))
        if len(p1.split()) > 1 and len(p2.split()) > 1:
            JHUphrases.append((p1,p2,score))
        JHUall.append((p1,p2,score))
    #print len(JHUwords)
    return (JHUall,JHUwords,JHUphrases)

def getJHUspears(data,We,words):
    gold = []
    llm_s = []
    llm_sa = []
    add_s = []
    for i in data:
        gold.append(i[2])
        llm_s.append(llm(i[0],i[1],words,We))
        llm_sa.append(absllm(i[0],i[1],words,We))
        add_s.append(add(i[0],i[1],words,We))
    return (spearmanr(gold,llm_s)[0], spearmanr(gold,add_s)[0], spearmanr(gold,llm_sa)[0])

def evaluateJHUPPDB(We,words):
    (JHUall,JHUwords,JHUphrases) = processJHUPPDB()
    jall = getJHUspears(JHUall,We,words)
    jwords = getJHUspears(JHUwords,We,words)
    jphrases = getJHUspears(JHUphrases,We,words)
    return (jall,jwords,jphrases)

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff

    if xdiff2 == 0 or ydiff2 == 0:
        return 0.0

    return diffprod / math.sqrt(xdiff2 * ydiff2)

def EvalSingleSystem(testlabelfile, sysscores):

    # read in golden labels
    goldlabels = []
    goldscores = []

    hasscore = False
    with open(testlabelfile) as tf:
        for tline in tf:
            tline = tline.strip()
            tcols = tline.split('\t')
            if len(tcols) == 2:
                goldscores.append(float(tcols[1]))
                if tcols[0] == "true":
                    goldlabels.append(True)
                elif tcols[0] == "false":
                    goldlabels.append(False)
                else:
                    goldlabels.append(None)

    tp = 0
    fn = 0
    # evaluation metrics
    for i in range(len(goldlabels)):

        if goldlabels[i] == True:
            tp += 1

    # system degreed scores vs golden binary labels
    # maxF1 / Precision / Recall

    maxF1 = 0
    P_maxF1 = 0
    R_maxF1 = 0

    # rank system outputs according to the probabilities predicted
    sortedindex = sorted(range(len(sysscores)), key = sysscores.__getitem__)
    sortedindex.reverse()

    truepos  = 0
    falsepos = 0

    for sortedi in sortedindex:
        if goldlabels[sortedi] == True:
            truepos += 1
        elif goldlabels[sortedi] == False:
            falsepos += 1

        precision = 0

        if truepos + falsepos > 0:
            precision = float(truepos) / (truepos + falsepos)

        recall = float(truepos) / (tp + fn)
        f1 = 0

        #print truepos, falsepos, precision, recall

        if precision + recall > 0:
            f1 = 2 * precision * recall / (precision + recall)
            if f1 > maxF1:
                maxF1 = f1
                P_maxF1 = precision
                R_maxF1 = recall

    # system degreed scores  vs golden degreed scores
    # Pearson correlation
    #print len(sysscores), len(goldscores)
    pcorrelation = pearson(sysscores, goldscores)

    return (pcorrelation, maxF1)

def evaluateTwitter(We,words):
    file = open('../dataset/test.data','r')
    lines = file.readlines()
    llm_scores = []
    allm_scores = []
    add_scores = []
    for i in lines:
        i =i.split("\t")
        s1 = i[2].lower()
        s2 = i[3].lower()
        llm_scores.append(llm(s1,s2,words,We,False))
        allm_scores.append(absllm(s1,s2,words,We,False))
        add_scores.append(add(s1,s2,words,We,False))
    (llm_c, llm_f) = EvalSingleSystem("../dataset/test.label",llm_scores)
    (add_c, add_f) = EvalSingleSystem("../dataset/test.label",add_scores)
    (allm_c, allm_f) = EvalSingleSystem("../dataset/test.label",allm_scores)
    return (llm_c, add_c, allm_c, llm_f, add_f, allm_f)

def getwordsim(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[2]))
            examples.append(ex)
    return examples

def getsimlex(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[3]))
            examples.append(ex)
    return examples

def getCorr(examples, We, words):
    gold = []
    pred = []
    num_unks = 0
    for i in examples:
        (v1,t1) = lookup_with_unk(We,words,i[0])
        (v2,t2) = lookup_with_unk(We,words,i[1])
        #print v1,v2
        pred.append(-1*cosine(v1,v2)+1)
        if t1:
            num_unks += 1
            #print i[0]
        if t2:
            num_unks += 1
        gold.append(i[2])
    return (spearmanr(pred,gold)[0], num_unks)

def evaluateWordSim(We, words):
    ws353ex = getwordsim('../dataset/wordsim353.txt')
    ws353sim = getwordsim('../dataset/wordsim-sim.txt')
    ws353rel = getwordsim('../dataset/wordsim-rel.txt')
    simlex = getsimlex('../dataset/SimLex-999.txt')
    (c1,u1) = getCorr(ws353ex,We,words)
    (c2,u2) = getCorr(ws353sim,We,words)
    (c3,u3) = getCorr(ws353rel,We,words)
    (c4,u4) = getCorr(simlex,We,words)
    return ([c1,c2,c3,c4],[u1,u2,u3,4])

def getTokens(p1,p2,tokenize):
    t1 = nltk.word_tokenize(p1)
    t1 = tokenFix(t1)
    if len(t1) == 0 or tokenize == False:
        t1 = p1.split()
    t2 = nltk.word_tokenize(p2)
    t2 = tokenFix(t2)
    if len(t2) == 0 or tokenize == False:
        t2 = p2.split()
    return t1, t2

def llm(p1,p2,words,We,tokenize=True):
    p1,p2 = getTokens(p1,p2,tokenize)
    total = 0
    for i in p1:
        v1 = lookup(We,words,i)
        max = -5
        for j in p2:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score = 0.5*total / len(p1)
    total = 0
    for i in p2:
        v1 = lookup(We,words,i)
        max = -5
        for j in p1:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score += 0.5*total / float(len(p2))
    return llm_score

def absllm(p1,p2,words,We,tokenize=True):
    p1,p2 = getTokens(p1,p2,tokenize)
    total = 0
    for i in p1:
        v1 = lookup(We,words,i)
        max = 0
        for j in p2:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(abs(score) > abs(max)):
                max = score
        total += max
    llm_score = 0.5*total / len(p1)
    total = 0
    for i in p2:
        v1 = lookup(We,words,i)
        max = 0
        for j in p1:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(abs(score) > abs(max)):
                max = score
        total += max
    llm_score += 0.5*total / len(p2)
    return llm_score

def add(p1,p2,words,We,tokenize=True):
    p1,p2 = getTokens(p1,p2,tokenize)
    accumulator = np.zeros(lookup(We,words,p1[0]).shape)
    for i in p1:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p1_emb = accumulator / len(p1)
    accumulator = np.zeros(lookup(We,words,p2[0]).shape)
    for i in p2:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p2_emb = accumulator / len(p1)
    return -1*cosine(p1_emb,p2_emb)+1


def scoreannoppdb(f,We,words):
    f = open(f,'r')
    lines = f.readlines()
    allm_preds = []
    llm_preds = []
    add_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        i=i.split('|||')
        (p1,p2,score) = (i[0].strip(),i[1].strip(),float(i[2]))
        llm_preds.append(llm(p1,p2,words,We))
        allm_preds.append(absllm(p1,p2,words,We))
        add_preds.append(add(p1,p2,words,We))
        gold.append(score)
    return (spearmanr(llm_preds,gold)[0], spearmanr(add_preds,gold)[0], spearmanr(allm_preds,gold)[0])

def tokenFix(toks):
    arr = []
    for i in toks:
        if len(i) > 1:
            i = i.replace(",","")
            arr.append(i)
    return arr

def scoreSE(f,We,words):
    f = open(f,'r')
    lines = f.readlines()
    lines.pop(0)
    examples=[]
    llm_preds = []
    add_preds = []
    allm_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        i=i.split('\t')
        (p1,p2,score) = (i[1].strip(),i[2].strip(),float(i[3]))
        llm_preds.append(llm(p1,p2,words,We))
        add_preds.append(add(p1,p2,words,We))
        allm_preds.append(absllm(p1,p2,words,We))
        gold.append(score)
    return (spearmanr(llm_preds,gold)[0], spearmanr(add_preds,gold)[0], spearmanr(allm_preds,gold)[0])

def evaluateAnno(We, words):
    return scoreannoppdb('../dataset/ppdb_test.txt',We,words)

def evaluateSE(We, words):
    return scoreSE('../dataset/SICK_trial.txt',We,words)

def evaluate_adagrad(We,words):
    (corr, unk) = evaluateWordSim(We,words)
    (llm_s, add_s, allm_s) = scoreannoppdb('../dataset/ppdb_test.txt',We,words)
    (llm_s2, add_s2, allm_s2) = scoreannoppdb('../dataset/ppdb_dev.txt',We,words)
    (llm_se, add_se, allm_se) = evaluateSE(We,words)
    ((j1,j2,j3),(j4,j5,j6),(j7,j8,j9)) = evaluateJHUPPDB(We,words)
    (llm_tw, add_tw, allm_tw, _, _, _) = evaluateTwitter(We,words)
    s = "wsim: {0:.4f} {1:.4f} {2:.4f} {3:.4f} "
    s += "ppdb test: {4:.4f} {5:.4f} {6:.4f} "
    s += "semeval: {7:.4f} {8:.4f} {9:.4f} "
    s += "JHU: all {10:.4f} {11:.4f} {12:.4f} | word {13:.4f} | phrase {14:.4f} {15:.4f} {16:.4f} "
    s += "Twitter: {17:.4f} {18:.4f} {19:.4f} "
    s += "ppdb dev: {20:.4f} {21:.4f} {22:.4f}"
    s=s.format(corr[0], corr[1], corr[2], corr[3], llm_s, add_s, allm_s, llm_se, add_se, allm_se, j1, j2, j3, j4, j7, j8, j9, llm_tw, add_tw, allm_tw, llm_s2, add_s2, allm_s2)
    print s

def getBin(p1,p2):
    t1 = nltk.word_tokenize(p1)
    t2 = nltk.word_tokenize(p2)
    return np.maximum(len(t1),len(t2))

def scoreSE(model,words):
    f = open('../dataset/SICK_test_annotated.txt','r')
    lines = f.readlines()
    lines.pop(0)
    l6_gold = []
    l6_pred = []
    l8_gold = []
    l8_pred = []
    l10_gold = []
    l10_pred = []
    gold = []
    pred = []
    for i in lines:
        i=i.strip()
        i=i.split('\t')
        (p1,p2,score) = (i[1].strip(),i[2].strip(),float(i[3]))
        pred_score = add(p1,p2,words,model)
        bin = getBin(p1,p2)
        if bin <= 6:
            l6_pred.append(pred_score)
            l6_gold.append(score)
        if bin <= 8:
            l8_pred.append(pred_score)
            l8_gold.append(score)
        if bin <= 10:
            l10_pred.append(pred_score)
            l10_gold.append(score)
        pred.append(pred_score)
        gold.append(score)
    #print rnn_preds, gold
    print len(l6_gold),len(l8_gold),len(l10_gold),len(gold)
    return spearmanr(l6_pred,l6_gold)[0], spearmanr(l8_pred,l8_gold)[0], spearmanr(l10_pred,l10_gold)[0], spearmanr(pred,gold)[0]

if __name__ == "__main__":
    #(words, We) = getWordmap('../data/skipwiki25.txt')
    #evaluate_adagrad(We,words)
    #(words, We) = getWordmap('../data/paragram_vectors.txt')
    #evaluate_adagrad(We,words)
    (words, We) = getWordmap('../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt')
    print scoreSE(We,words)
    print evaluateTwitter(We,words)
    #evaluate_adagrad(We,words)

