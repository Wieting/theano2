
class params(object):

    def __init__(self, lam=1E-5, dataf='../data/ppdb-words.txt',
        batchsize=100, margin=1, epochs=10, eta = 0.20, evaluate=True, save=False,
        hiddensize=25, outfile="test.out", type = "RAND"):

        self.lam=lam
        self.dataf = dataf
        self.batchsize = batchsize
        self.margin = margin
        self.epochs = epochs
        self.eta = eta
        self.evaluate = evaluate
        self.save = save
        self.data = []
        self.hiddensize = hiddensize
        self.outfile = outfile
        self.type = type