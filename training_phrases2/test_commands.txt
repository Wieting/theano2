sh exp1.train_gouda.sh 1E-5 0.0001 phrase 100 300 50 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt lstm 300 simlex ../data/phrase_pairs MAX 0.2 True True
sh exp1.train_gouda.sh 1E-5 0.0001 phrase 100 300 50 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt lstm 300 simlex ../data/phrase_pairs MAX 0.2 True False
sh exp1.train_gouda.sh 1E-5 0.0001 phrase 100 300 50 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt lstm 300 simlex ../data/phrase_pairs MAX 0.2 False True
sh exp1.train_gouda.sh 1E-5 0.0001 phrase 100 300 50 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt lstm 300 simlex ../data/phrase_pairs MAX 0.2 False False
sh exp1.train_gouda.sh 1E-5 1e-06 phrase 400 300 50 ../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt lstm 300 wordsim ../data/phrase_pairs MIX 0.6 True True
sh exp1.train_gouda.sh 1E-5 1e-06 phrase 400 300 50 ../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt lstm 300 wordsim ../data/phrase_pairs MIX 0.6 True False
sh exp1.train_gouda.sh 1E-5 1e-06 phrase 400 300 50 ../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt lstm 300 wordsim ../data/phrase_pairs MIX 0.6 False True
sh exp1.train_gouda.sh 1E-5 1e-06 phrase 400 300 50 ../data/ppdbxlcc.1e-06.25.0.8.MAX.txt.params.38.txt lstm 300 wordsim ../data/phrase_pairs MIX 0.6 False False