import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/phrase_pairs'
mem = [100]
lamC = [1E-4,1E-5,1E-6]
bs = [100,200,400]
margins = [0.2, 0.4, 0.6]
hidden = [300]
types = ['MAX','MIX']

'''
for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for m in range(len(mem)):
                    for t in types:
                        vars = (lamC[l],fname,bs[j],dim,mem[m],wordfile,'lstm',hidden[h],wordstem,data,t,margins[c])
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False".format(*vars)
                        print cmd

'''
#mem = [100,200,300]

for j in range(len(bs)):
    for l in range(len(lamC)):
        for c in range(len(margins)):
            for h in range(len(hidden)):
                for m in range(len(mem)):
                    for t in types:
                        vars = (lamC[l],fname,bs[j],dim,mem[m],wordfile,'extra',hidden[h],wordstem,data,t,margins[c])
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True True".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} True False".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False True".format(*vars)
                        print cmd
                        cmd = "sh exp1.train_gouda.sh 1E-5 {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} False False".format(*vars)
                        print cmd
