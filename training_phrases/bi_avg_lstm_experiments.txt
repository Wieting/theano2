sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-04 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX lstm


sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-09 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX lstm
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX lstm



sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-04 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi


sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-09 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi



sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-04 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX avg


sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-09 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX avg



sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-04 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi_avg


sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-07 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-05 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-06 1.0 phrase ../data/phrase_pairs 100 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg

sh train_bi_avg_lstm_experiments.sh 1e-05 1e-09 1.0 phrase ../data/phrase_pairs 400 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.4 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 200 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi_avg
sh train_bi_avg_lstm_experiments.sh 1e-05 1e-08 1.0 phrase ../data/phrase_pairs 50 300 ../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt MAX 0.6 ppdbxlcc.1e-09.50.0.6.MIX bi_avg