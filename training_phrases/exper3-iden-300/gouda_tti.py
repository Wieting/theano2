import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
wordfile = sys.argv[4]
wordstem = sys.argv[5]
dim = sys.argv[6]

lambdaWe=[1E-5]
lamC = [1E-3,1E-4,1E-5,1E-6,1E-7]
bs=[50,100,200,400]
margins = [0.4,0.6,0.8,1.0,2.0]
type = ["MAX"]

for j in range(len(bs)):
    for i in range(len(lambdaWe)):
        for k in range(len(type)):
            for l in range(len(lamC)):
                for c in range(len(margins)):
                    vars = (lambdaWe[i],lamC[l],frac,fname,data,bs[j],dim,wordfile,type[k],margins[c],wordstem)
                    cmd = "sh exp3.train_gouda.sh {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}".format(*vars)
                    print cmd
