import cPickle
from scipy.stats import spearmanr
from scipy.spatial.distance import cosine
import nltk
import numpy as np
import pdb

data = cPickle.load(file("msr.data", 'rb'))

train_reps, test_reps, scores_train, scores_test, len_train, len_test = data

ptr = []; gtr = []
for i in range(len(train_reps)):
    r1 = train_reps[i][0]
    r2 = train_reps[i][1]
    pred = -cosine(r1,r2)+1
    score = scores_train[i]
    ptr.append(pred)
    gtr.append(score)

pte = []; gte = []
for i in range(len(test_reps)):
    r1 = test_reps[i][0]
    r2 = test_reps[i][1]
    pred = -cosine(r1,r2)+1
    score = scores_test[i]
    pte.append(pred)
    gte.append(score)


#find threshold

best = -1
acc_best = 0
for i in range(len(ptr)):
    if(len_train[i] > 9):
        continue
    print ptr[i], gtr[i]
    s = ptr[i]
    tc = 0
    t = 0
    for j in range(len(ptr)):
        if(len_train[j] > 9):
            continue
        t += 1
        if ptr[j] <= s:
            if gtr[j] < 1:
                tc += 1
        else:
            if gtr[j] > 0:
                tc += 1
    acc = tc/float(t)
    if acc > acc_best:
        acc_best = acc
        best = s

t = 0
tc = 0
for i in range(len(pte)):
    if(len_test[i] > 9):
        continue
    t += 1
    if pte[i] <= best:
        if gte[i] < 1:
            tc += 1
    else:
        if gte[i] > 0:
            tc += 1
print tc/float(t)
print best, acc_best