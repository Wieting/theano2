import cPickle
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from scipy.spatial.distance import cosine
import nltk
import numpy as np
import pdb

f = open('semeval_datasets.txt','r')
lines = f.readlines()

for i in lines:
    i=i.split()
    if not len(i) == 2:
        continue
    textf = i[0]; goldf = i[1]
    dataf = textf+".data"
    data = cPickle.load(file(dataf, 'rb'))
    test_reps, scores_test, len_test = data
    p = []; g = []
    for i in range(len(test_reps)):
        l = len_test[i]
        r1 = test_reps[i][0]
        r2 = test_reps[i][1]
        pred = -cosine(r1,r2)+1
        score = scores_test[i]
        p.append(pred)
        g.append(score)
    print textf, pearsonr(p,g)

print ""

for i in lines:
    i=i.split()
    if not len(i) == 2:
        continue
    textf = i[0]; goldf = i[1]
    dataf = textf+".add.data"
    data = cPickle.load(file(dataf, 'rb'))
    test_reps, scores_test, len_test = data
    p = []; g = []
    for i in range(len(test_reps)):
        l = len_test[i]
        r1 = test_reps[i][0]
        r2 = test_reps[i][1]
        pred = -cosine(r1,r2)+1
        score = scores_test[i]
        p.append(pred)
        g.append(score)
    print textf, pearsonr(p,g)