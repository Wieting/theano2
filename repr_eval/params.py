
class params(object):

    def __init__(self, LW=1E-5, LC=0.0001, dataf='../data/phrase_pairs',
        batchsize=100, margin=1, epochs=2, eta = 0.050, evaluate=True, save=False,
        hiddensize=25, outfile="test.out", type = "MAX", wordfile = '../data/skipwiki25.txt'):

        self.LW=LW
        self.LC=LC
        self.dataf = dataf
        self.batchsize = batchsize
        self.margin = margin
        self.epochs = epochs
        self.eta = eta
        self.evaluate = evaluate
        self.save = save
        self.data = []
        self.hiddensize = hiddensize
        self.outfile = outfile
        self.type = type
        self.wordfile = wordfile
