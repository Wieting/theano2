import cPickle
from scipy.stats import spearmanr
from scipy.spatial.distance import cosine
import nltk
import numpy as np
import pdb

def getTokens(p1,p2,tokenize):
    t1 = nltk.word_tokenize(p1)
    t1 = tokenFix(t1)
    if len(t1) == 0 or tokenize == False:
        t1 = p1.split()
    t2 = nltk.word_tokenize(p2)
    t2 = tokenFix(t2)
    if len(t2) == 0 or tokenize == False:
        t2 = p2.split()
    #print t1
    #print t2
    return t1, t2

def tokenFix(toks):
    arr = []
    for i in toks:
        if len(i) > 1:
            i = i.replace(",","")
            arr.append(i)
    return arr


f = open('../dataset/SICK_test_annotated.txt','r')
lines = f.readlines()
lines.pop(0)

d = []

for i in lines:
    i=i.strip()
    i=i.split('\t')
    ex = (i[1].strip(),i[2].strip(),float(i[3]))
    t1,t2 = getTokens(ex[0],ex[1],True)
    n = max(len(t1),len(t2))
    d.append(n)

hist = np.histogram(d,range(30))
print hist


data = cPickle.load(file("se.add.data", 'rb'))

train_reps, test_reps, scores_train, scores_test, len_train, len_test = data

p = []; g = []
for i in range(len(test_reps)):
    l = len_test[i]
    r1 = test_reps[i][0]
    r2 = test_reps[i][1]
    pred = -cosine(r1,r2)+1
    score = scores_test[i]
    if l == 7:
        p.append(pred)
        g.append(score)
print spearmanr(p,g)