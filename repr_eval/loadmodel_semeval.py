
import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
import lasagne
import sys
import cPickle
import utils
from evaluate_ff import evaluate_all
import evaluate_ff

def prepare_data(list_of_seqs):
    lengths = [len(s) for s in list_of_seqs]
    n_samples = len(list_of_seqs)
    maxlen = np.max(lengths)
    x = np.zeros((n_samples, maxlen)).astype('int32')
    x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
    for idx, s in enumerate(list_of_seqs):
        x[idx, :lengths[idx]] = s
        x_mask[idx, :lengths[idx]] = 1.
    return x, x_mask

p = cPickle.load(file("../../../phrase.1e-05.1e-06.400.MAX.0.3.300.simlex.txt3.pickle", 'rb'))
(words, We) = utils.getWordmap("../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt")

#0 W_in_to_ingate
#1 W_hid_to_ingate
#2 b_ingate
#3 W_in_to_forgetgate
#4 W_hid_to_forgetgate
#5 b_forgetgate
#6 W_in_to_cell
#7 W_hid_to_cell
#8 b_cell
#9 W_in_to_outgate
#10 W_hid_to_outgate
#11 b_outgate
#12 W_cell_to_ingate
#13 W_cell_to_forgetgate
#14 W_cell_to_outgate

batchindices = T.imatrix()
mask = T.matrix()

ingate = lasagne.layers.Gate(W_in=p[0], W_hid=p[1], W_cell=p[12], b=p[2])
forgetgate = lasagne.layers.Gate(W_in=p[3], W_hid=p[4], W_cell=p[13], b=p[5])
outgate = lasagne.layers.Gate(W_in=p[9], W_hid=p[10], W_cell=p[14], b=p[11])
cell = lasagne.layers.Gate(W_in=p[6], W_hid=p[7], W_cell=None, b=p[8], nonlinearity=lasagne.nonlinearities.tanh)

We = theano.shared(We).astype(config.floatX)
l_in = lasagne.layers.InputLayer((None, None, 1))
l_mask = lasagne.layers.InputLayer(shape=(None, None))
l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
l_lstm = lasagne.layers.LSTMLayer(l_emb, 500, ingate = ingate, forgetgate = forgetgate,
                                  outgate = outgate, cell = cell, peepholes=True, learn_init=False, mask_input = l_mask)
l_out = lasagne.layers.SliceLayer(l_lstm, -1, 1)

emb = lasagne.layers.get_output(l_out, {l_in:batchindices, l_mask:mask})
ff = theano.function([batchindices,mask], emb)

f = open('semeval_datasets.txt','r')
lines = f.readlines()

for i in lines:
    i=i.split()
    if not len(i) == 2:
        continue
    textf = i[0]; goldf = i[1]
    fout = textf+".add.data"
    #fout = textf+".data"
    #test_reps, scores_test, len_test = evaluate_ff.getAllRepresentations(textf,goldf,ff,words)
    test_reps, scores_test, len_test = evaluate_ff.getAllAddRepresentations(textf,goldf,ff,words,We.get_value())

    f = file(fout, 'wb')
    cPickle.dump((test_reps, scores_test, len_test), f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()