
import cPickle
import theano
import lasagne
from theano import tensor as T
import time
import numpy as np
import pdb
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.spatial.distance import cosine
import numpy as np

def get_minibatches_idx(n, minibatch_size, shuffle=False):
    idx_list = np.arange(n, dtype="int32")

    if shuffle:
        np.random.shuffle(idx_list)

    minibatches = []
    minibatch_start = 0
    for i in range(n // minibatch_size):
        minibatches.append(idx_list[minibatch_start:
        minibatch_start + minibatch_size])
        minibatch_start += minibatch_size

    if (minibatch_start != n):
        # Make a minibatch out of what is left
        minibatches.append(idx_list[minibatch_start:])

    return zip(range(len(minibatches)), minibatches)

# return 2 matrices, the first is
def getData(train_representations, train_scores):
    rep1 = []
    rep2 = []
    for i in train_representations:
        e1 = i[0][0].tolist(); e2 = i[1][0].tolist()
        rep1.append(e1)
        rep2.append(e2)
    return rep1, rep2, train_scores

def convert_to_distribution(labels):
    m = np.zeros((len(labels),2))
    for i in range(len(labels)):
        m[i][0]=1-labels[i]
        m[i][1]=labels[i]
    m = m + 0.00001
    #pdb.set_trace()
    return m

'''
data = cPickle.load(file("twitter.add.data", 'rb'))
pred = []
for i in data[1]:
    v1 = i[0]
    v2 = i[1]
    s = -cosine(v1,v2)+1
    pred.append(s)
print pearsonr(pred,data[3])
'''

data = cPickle.load(file("twitter.add.data", 'rb'))
train_reps,test_reps,train_scores,test_scores, train_lengths, test_lengths = data

hist = np.histogram(test_lengths,range(30))
print hist

for n in range(20):
    p = []; g = []; ct = 0
    for i in range(len(test_reps)):
        l = test_lengths[i]
        r1 = test_reps[i][0]
        r2 = test_reps[i][1]
        pred = -cosine(r1,r2)+1
        score = test_scores[i]
        if l == n:
            p.append(pred)
            g.append(score)
            ct += 1
    print n, ct, pearsonr(p,g)

"""
dim = 500
batchsize = 25
epochs = 20
eta = 0.05
L=0.0005

e1 = T.matrix(); e2 = T.matrix()
Y = T.matrix()

dot_v = e1*e2
abs_v = abs(e1-e2)
lin_dot = lasagne.layers.InputLayer((None, dim))
lin_abs = lasagne.layers.InputLayer((None, dim))
l_sum = lasagne.layers.ConcatLayer([lin_dot, lin_abs])
#l_sigmoid = lasagne.layers.DenseLayer(l_sum, 1, nonlinearity=lasagne.nonlinearities.linear)
#X = lasagne.layers.get_output(l_sigmoid, {lin_dot:dot_v, lin_abs:abs_v})
#cost = (Y - X)**2
l_sigmoid = lasagne.layers.DenseLayer(l_sum, 10, nonlinearity=lasagne.nonlinearities.sigmoid)
l_softmax = lasagne.layers.DenseLayer(l_sigmoid, 2, nonlinearity=T.nnet.softmax)
X = lasagne.layers.get_output(l_softmax, {lin_dot:dot_v, lin_abs:abs_v})
M = T.log(X)
cost = Y*(T.log(Y) - M)
cost = cost.sum(axis=1)/2.
X = X[:,1]

e1e2 = (e1*e2).sum(axis=1)
e1e2norm = T.sqrt(T.sum(e1**2,axis=1)) * T.sqrt(T.sum(e2**2,axis=1))
cosine = e1e2 / e1e2norm

#all_params = lasagne.layers.get_all_params(l_sigmoid, trainable=True)
all_params = lasagne.layers.get_all_params(l_softmax, trainable=True)
l2 = L*sum(lasagne.regularization.l2(x) for x in all_params)
cost = T.mean(cost)+l2

rep1, rep2, labels = getData(data[0],data[2])
test1, test2, test_l = getData(data[1],data[3])
#pdb.set_trace()

updates = lasagne.updates.adagrad(cost, all_params, eta)
train_function = theano.function([e1,e2,Y], cost, updates=updates)
feedforward_function = theano.function([e1,e2],X)


cosine_function = theano.function([e1,e2],cosine)

try:
    start_time = time.time()
    for eidx in xrange(epochs):
        n_samples = 0

        # Get new shuffled index for the training set.
        kf = get_minibatches_idx(len(labels), batchsize, shuffle=True)
        uidx = 0
        for _, train_index in kf:

            batch_1 = np.matrix([rep1[t] for t in train_index])
            batch_2 = np.matrix([rep2[t] for t in train_index])
            #batch_l = np.transpose(np.matrix([labels[t] for t in train_index]))
            batch_l = convert_to_distribution([labels[t] for t in train_index])

            #pdb.set_trace()

            t1 = time.time()
            cost = train_function(batch_1, batch_2, batch_l)
            t2 = time.time()
            #print "cost time: "+str(t2-t1)

            if np.isnan(cost) or np.isinf(cost):
                print 'NaN detected'

            #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

            uidx += 1

        X = feedforward_function(rep1,rep2)
        #pdb.set_trace()
        print pearsonr(X.squeeze(),labels)

        X = feedforward_function(test1,test2)
        #pdb.set_trace()
        print pearsonr(X.squeeze(),test_l)

        X = cosine_function(test1,test2)
        print pearsonr(X.squeeze(),test_l)

        print 'Epoch ', (eidx+1), 'Cost ', cost

    print 'Seen %d samples' % n_samples

except KeyboardInterrupt:
    print "Training interupted"

end_time = time.time()
print "total time:", (end_time - start_time)
"""