import cPickle
from scipy.stats import spearmanr
from scipy.spatial.distance import cosine

def getCorr(rep,scores):
    pred = []
    for i in rep:
        v1 = i[0]
        v2 = i[1]
        s = -cosine(v1,v2)+1
        pred.append(s)
    print spearmanr(pred,scores)

data = cPickle.load(file("mlpara.data", 'rb'))
print len(data[0]), len(data[3])
getCorr(data[0],data[3])
getCorr(data[1],data[4])
getCorr(data[2],data[5])