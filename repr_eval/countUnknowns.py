
import cPickle
import utils
import evaluate_ff

f = open('semeval_datasets.txt','r')
lines = f.readlines()

p = cPickle.load(file("../../../phrase.1e-05.1e-06.400.MAX.0.3.300.simlex.txt3.pickle", 'rb'))
(words, We) = utils.getWordmap("../data/ppdbxlcc.1e-09.50.0.6.MIX.txt.params.35.txt")

for i in lines:
    i=i.split()
    if not len(i) == 2:
        continue
    textf = i[0]
    evaluate_ff.countAllUnknowns(textf,words,We)
